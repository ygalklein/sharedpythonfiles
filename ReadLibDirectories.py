﻿import os
import sys
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('ReadLibDirectories')

def main(filePath):
    
    libs = {}
        
    if not os.path.isfile(filePath):
        logger.error("Illegal system libraries file '%s'!" % filePath)
        sys.exit(1)
    
    with open(filePath, 'r') as libsFile:
        stripList = lambda lst: [t for t in list(map(lambda s: s.strip(), lst)) if t]
        for line in libsFile:
            # skip empty lines
            if not line.strip():
                continue
        
            lib, path = stripList(line.split())
            libs[lib] = path
    
    if len(libs) == 0:
        logger.error("System libraries file '%s' is empty!" % filePath)
        sys.exit(1)

    return(libs)
