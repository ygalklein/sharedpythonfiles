﻿import os
import sys
import subprocess
import logging
import inspect
import shutil

from ReadDeploymentFiles import readDeploymentFiles
from git import getGitHash, getGitDiff

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('Dependencies')
projects = None
root_dir = None


def remove(codeDirectory="", deploymentFilePath = ""):
    logger.info(f'Removing sources of dependencies...')

    global root_dir, projects
    if root_dir is None: root_dir = codeDirectory
    if projects is None: readDeploymentFiles(deploymentFilePath)

    for project in projects:
        cloned_dir = os.path.join(root_dir, 'src', project.name.lower())
        diff = getGitDiff(cloned_dir)
        if len(diff)>0:
            logger.info(f'Dependency {project.name} was not removed due to diff from git repository')
        else:
            shutil.rmtree(cloned_dir)
            
    
def clone(codeDirectory, deploymentFilePath, diff = False):
    global root_dir, projects
    if root_dir is None: root_dir = codeDirectory       
    projects = readDeploymentFiles(deploymentFilePath)

    for project in projects:
        logger.info("Importing '%s'" % project.name)
        clonedDirectory = os.path.join(root_dir, 'src', project.name.lower())
        if os.path.isdir(clonedDirectory):
            logger.info("Project '%s' already exists" % project.name)        
            hash = getGitHash(clonedDirectory)
            if project.hash != hash:
                logger.error("The commit of '%s' does not match the record in '%s'" % (project.name, deploymentFilePath))
                sys.exit(1)
            
            diff = diff or len(getGitDiff(clonedDirectory)) > 0
            continue
    
        logger.debug(f'Cloning project `{project.name}` from owner `{project.owner}`')
        cmd = ['git', 'clone', f'git@gitlab.com:{project.owner}/{project.name}.git', clonedDirectory]
        clonning = subprocess.run(cmd,
                                  stdout=subprocess.PIPE,
                                  stderr=subprocess.STDOUT)
        if not clonning.returncode == 0:
            logger.error("Cloning project '%s' failed." % project.name)
            logger.error("cmd = '%s'" % ' '.join(cmd))
            sys.exit(1)
            
        if getGitHash(clonedDirectory) == project.hash:
            continue
        
        branchName = '%s_%s' % (project.name, project.hash)
        logger.debug("checking out new branch '%s'" % branchName)
        cmd = ['git', 'checkout', '-b', branchName, project.hash]
        checkout = subprocess.run(cmd,
                                  stdout=subprocess.PIPE,
                                  stderr=subprocess.STDOUT,
                                  cwd=clonedDirectory)
        if not checkout.returncode == 0:
            logger.error("Checkout of hash '%s' in project '%s' failed."
                         "There is probably no such hash." % (project.hash, project.name))
            logger.error("cmd = '%s'" % ' '.join(cmd))
            sys.exit(1)
    
    return diff
