﻿class Project:
	def __init__(self, name, owner, hash):
		self.name = name
		self.owner = owner
		self.hash = hash

def readDeploymentFiles(deploymentFilePath):
	projects = []
	with open(deploymentFilePath, 'r') as deploymentFile:
		for line in deploymentFile:
			
			# skip empty lines
			if not line.strip():
				continue
				
			stripList = lambda lst: [t for t in list(map(lambda s: s.strip(), lst)) if t]
			name, details = stripList(line.split('='))
			owner, hash = stripList(details.split(','))
			projects.append(Project(name, owner, hash))

	return projects

if __name__ == "__main__":
	print(main(".."))