import subprocess
import logging
import tempfile
import sys

def getGitDiff(workingDirectory):
	_, diffFile = tempfile.mkstemp()
	cmd = ['git', 'diff']
	gitLog = subprocess.run(cmd,
							stdout=open(diffFile,'w'),
							cwd=workingDirectory)
	
	with open(diffFile, 'r') as difference:
		return difference.readlines()

def getGitHash(workingDirectory):
	logger = logging.getLogger('getGitHash')
	cmd = ['git', 'log']
	gitLog = subprocess.run(cmd,
							stdout=subprocess.PIPE,
							stderr=subprocess.STDOUT,
							cwd=workingDirectory)
	logFile = gitLog.stdout.decode()

	if logFile.startswith('fatal: Not a git repository'):
		logger.error("The working dorectory is not a git repository!")
		sys.exit(1)
		
	return logFile.split()[1]