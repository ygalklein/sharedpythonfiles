import os


def compare_old(verfilename, newver):
    if not os.path.exists(verfilename):
        return False
    with open(verfilename, "r") as oldverfile:
        oldver = oldverfile.read()
    return oldver == newver