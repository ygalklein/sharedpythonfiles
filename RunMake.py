﻿import os
import subprocess

def main(build_dir, config):
    config_build_dir = os.path.join(build_dir, config)
    cmd = ['make', '-j']
    make = subprocess.run(cmd,
                          stdout=open(os.path.join(build_dir, config, "_build.out"), 'w'),
                          stderr=open(os.path.join(build_dir, config, "_build.err"), 'w'),
                          #stdin=open('/dev/null'),
                          cwd=config_build_dir)
    return make
    
