﻿import os
import tempfile
import subprocess

def module(lmod_link, command, *modules):
	"""
	Wrapper for lmod (module) commands in python
	
	command -- load, unload, etc.
	modules -- module names (optional)
	"""

	_, commandsFile = tempfile.mkstemp()
	
	modules = ' '.join(modules)
	cmd = [lmod_link, 'python', command, modules]
	subprocess.run(cmd,
				   stdout=open(commandsFile, 'w'),
				   stdin=open('/dev/null'))
	
	with open(commandsFile, 'r') as module_commands:
		exec(module_commands.read())