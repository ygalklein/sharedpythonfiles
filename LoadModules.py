﻿import os
import sys
import logging
from lmod import module

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('LoadModules')

def main(lmod, modules_file):
    if not os.path.isfile(modules_file): 
        logger.error(f'Did not find a modules file `{modules_file}`')
        sys.exit(1)
        
    with open(modules_file, 'r') as module_lines:
        for m in module_lines:
            logger.info(f'loading module {m}')
            module(lmod, "load", m)