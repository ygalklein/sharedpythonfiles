﻿import os
import sys
import subprocess
import logging
import inspect

from ReadDeploymentFiles import readDeploymentFiles
from git import getGitHash, getGitDiff

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('ImportSharedLibraries')

def importSharedLibraries(codeDirectory, deploymentFilePath, diff = False):

	projects = readDeploymentFile(deploymentFilePath)

	for project in projects:
		logger.info("Importing '%s'" % project.name)
		clonedDirectory = os.path.join(codeDirectory, src, project.name.lower())
		if os.path.isdir(clonedDirectory):
			logger.info("Project '%s' already exists" % project.name)		
			hash = getGitHash(clonedDirectory)
			if project.hash != hash:
				logger.error("The commit of '%s' does not match the record in '%s'" % (project.name, deploymentFilePath))
				sys.exit(1)
			
			diff = diff or len(getGitDiff(clonedDirectory)) > 0
			continue
	
		logger.debug("Cloning lib '%s' from owner '%s'" % (project.name, project.owner))
		cmd = ['git', 'clone', "git@gitlab:%s/%s.git" % (project.owner, project.name), clonedDirectory]
		clonning = subprocess.run(cmd,
								  stdout=subprocess.PIPE,
								  stderr=subprocess.STDOUT)
		if not clonning.returncode == 0:
			logger.error("Cloning lib '%s' failed." % project.name)
			logger.error("cmd = '%s'" % ' '.join(cmd))
			sys.exit(1)
			
		if getGitHash(clonedDirectory) == project.hash:
			continue
		
		branchName = '%s_%s' % (project.name, project.hash)
		logger.debug("checking out new branch '%s'" % branchName)
		cmd = ['git', 'checkout', '-b', branchName, project.hash]
		checkout = subprocess.run(cmd,
								  stdout=subprocess.PIPE,
								  stderr=subprocess.STDOUT,
								  cwd=clonedDirectory)
		if not checkout.returncode == 0:
			logger.error("Checkout of hash '%s' in project '%s' failed."
						 "There is probably no such hash." % (project.hash, project.name))
			logger.error("cmd = '%s'" % ' '.join(cmd))
			sys.exit(1)
		
	return diff
